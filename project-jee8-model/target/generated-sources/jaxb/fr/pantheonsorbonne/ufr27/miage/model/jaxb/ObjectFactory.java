//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.01.09 à 03:01:53 AM CET 
//


package fr.pantheonsorbonne.ufr27.miage.model.jaxb;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.pantheonsorbonne.ufr27.miage.model.jaxb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.pantheonsorbonne.ufr27.miage.model.jaxb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Train }
     * 
     */
    public Train createTrain() {
        return new Train();
    }

    /**
     * Create an instance of {@link Arret }
     * 
     */
    public Arret createArret() {
        return new Arret();
    }

    /**
     * Create an instance of {@link Passager }
     * 
     */
    public Passager createPassager() {
        return new Passager();
    }

    /**
     * Create an instance of {@link Perturbation }
     * 
     */
    public Perturbation createPerturbation() {
        return new Perturbation();
    }

    /**
     * Create an instance of {@link TrainArret }
     * 
     */
    public TrainArret createTrainArret() {
        return new TrainArret();
    }

    /**
     * Create an instance of {@link TrainList }
     * 
     */
    public TrainList createTrainList() {
        return new TrainList();
    }

}
