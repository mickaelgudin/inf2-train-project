//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.01.09 à 03:01:53 AM CET 
//


package fr.pantheonsorbonne.ufr27.miage.model.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour arret complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="arret"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idArret" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="nomArret" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ville" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "arret", propOrder = {
    "idArret",
    "nomArret",
    "ville"
})
@XmlRootElement(name = "arret")
public class Arret {

    protected int idArret;
    @XmlElement(required = true)
    protected String nomArret;
    @XmlElement(required = true)
    protected String ville;

    /**
     * Obtient la valeur de la propriété idArret.
     * 
     */
    public int getIdArret() {
        return idArret;
    }

    /**
     * Définit la valeur de la propriété idArret.
     * 
     */
    public void setIdArret(int value) {
        this.idArret = value;
    }

    /**
     * Obtient la valeur de la propriété nomArret.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomArret() {
        return nomArret;
    }

    /**
     * Définit la valeur de la propriété nomArret.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomArret(String value) {
        this.nomArret = value;
    }

    /**
     * Obtient la valeur de la propriété ville.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVille() {
        return ville;
    }

    /**
     * Définit la valeur de la propriété ville.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVille(String value) {
        this.ville = value;
    }

    public Arret withIdArret(int value) {
        setIdArret(value);
        return this;
    }

    public Arret withNomArret(String value) {
        setNomArret(value);
        return this;
    }

    public Arret withVille(String value) {
        setVille(value);
        return this;
    }

}
