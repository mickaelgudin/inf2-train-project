//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.01.09 à 03:01:53 AM CET 
//


package fr.pantheonsorbonne.ufr27.miage.model.jaxb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour train complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="train"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="numeroTrain" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="localisation" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="arrete" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="reservation" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="typeDeTrain" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="trainArrets"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="trainArret" type="{}trainArret" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="perturbations"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="perturbation" type="{}perturbation" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="passagers"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="passager" type="{}passager" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "train", propOrder = {
    "numeroTrain",
    "localisation",
    "arrete",
    "reservation",
    "typeDeTrain",
    "trainArrets",
    "perturbations",
    "passagers"
})
@XmlRootElement(name = "train")
public class Train {

    @XmlElement(required = true)
    protected String numeroTrain;
    protected int localisation;
    protected boolean arrete;
    protected boolean reservation;
    @XmlElement(required = true)
    protected String typeDeTrain;
    @XmlElementWrapper(required = true)
    @XmlElement(name = "trainArret")
    protected List<TrainArret> trainArrets = new ArrayList<TrainArret>();
    @XmlElementWrapper(required = true)
    @XmlElement(name = "perturbation")
    protected List<Perturbation> perturbations = new ArrayList<Perturbation>();
    @XmlElementWrapper(required = true)
    @XmlElement(name = "passager")
    protected List<Passager> passagers = new ArrayList<Passager>();

    /**
     * Obtient la valeur de la propriété numeroTrain.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTrain() {
        return numeroTrain;
    }

    /**
     * Définit la valeur de la propriété numeroTrain.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTrain(String value) {
        this.numeroTrain = value;
    }

    /**
     * Obtient la valeur de la propriété localisation.
     * 
     */
    public int getLocalisation() {
        return localisation;
    }

    /**
     * Définit la valeur de la propriété localisation.
     * 
     */
    public void setLocalisation(int value) {
        this.localisation = value;
    }

    /**
     * Obtient la valeur de la propriété arrete.
     * 
     */
    public boolean isArrete() {
        return arrete;
    }

    /**
     * Définit la valeur de la propriété arrete.
     * 
     */
    public void setArrete(boolean value) {
        this.arrete = value;
    }

    /**
     * Obtient la valeur de la propriété reservation.
     * 
     */
    public boolean isReservation() {
        return reservation;
    }

    /**
     * Définit la valeur de la propriété reservation.
     * 
     */
    public void setReservation(boolean value) {
        this.reservation = value;
    }

    /**
     * Obtient la valeur de la propriété typeDeTrain.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeDeTrain() {
        return typeDeTrain;
    }

    /**
     * Définit la valeur de la propriété typeDeTrain.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeDeTrain(String value) {
        this.typeDeTrain = value;
    }

    public List<TrainArret> getTrainArrets() {
        return trainArrets;
    }

    public void setTrainArrets(List<TrainArret> trainArrets) {
        this.trainArrets = trainArrets;
    }

    public List<Perturbation> getPerturbations() {
        return perturbations;
    }

    public void setPerturbations(List<Perturbation> perturbations) {
        this.perturbations = perturbations;
    }

    public List<Passager> getPassagers() {
        return passagers;
    }

    public void setPassagers(List<Passager> passagers) {
        this.passagers = passagers;
    }

    public Train withNumeroTrain(String value) {
        setNumeroTrain(value);
        return this;
    }

    public Train withLocalisation(int value) {
        setLocalisation(value);
        return this;
    }

    public Train withArrete(boolean value) {
        setArrete(value);
        return this;
    }

    public Train withReservation(boolean value) {
        setReservation(value);
        return this;
    }

    public Train withTypeDeTrain(String value) {
        setTypeDeTrain(value);
        return this;
    }

    public Train withTrainArrets(TrainArret... values) {
        if (values!= null) {
            for (TrainArret value: values) {
                getTrainArrets().add(value);
            }
        }
        return this;
    }

    public Train withTrainArrets(Collection<TrainArret> values) {
        if (values!= null) {
            getTrainArrets().addAll(values);
        }
        return this;
    }

    public Train withTrainArrets(List<TrainArret> trainArrets) {
        setTrainArrets(trainArrets);
        return this;
    }

    public Train withPerturbations(Perturbation... values) {
        if (values!= null) {
            for (Perturbation value: values) {
                getPerturbations().add(value);
            }
        }
        return this;
    }

    public Train withPerturbations(Collection<Perturbation> values) {
        if (values!= null) {
            getPerturbations().addAll(values);
        }
        return this;
    }

    public Train withPerturbations(List<Perturbation> perturbations) {
        setPerturbations(perturbations);
        return this;
    }

    public Train withPassagers(Passager... values) {
        if (values!= null) {
            for (Passager value: values) {
                getPassagers().add(value);
            }
        }
        return this;
    }

    public Train withPassagers(Collection<Passager> values) {
        if (values!= null) {
            getPassagers().addAll(values);
        }
        return this;
    }

    public Train withPassagers(List<Passager> passagers) {
        setPassagers(passagers);
        return this;
    }

}
