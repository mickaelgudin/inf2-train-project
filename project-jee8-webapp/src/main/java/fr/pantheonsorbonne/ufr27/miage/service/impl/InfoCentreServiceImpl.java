package fr.pantheonsorbonne.ufr27.miage.service.impl;

import java.io.IOException;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import fr.pantheonsorbonne.ufr27.miage.Main;
import fr.pantheonsorbonne.ufr27.miage.dao.PassagerDAO;
import fr.pantheonsorbonne.ufr27.miage.dao.TrainArretDAO;
import fr.pantheonsorbonne.ufr27.miage.dao.TrainDAO;
import fr.pantheonsorbonne.ufr27.miage.exception.NoTrainException;
import fr.pantheonsorbonne.ufr27.miage.jms.utils.TrainMapperCustom;
import fr.pantheonsorbonne.ufr27.miage.jpa.Perturbation;
import fr.pantheonsorbonne.ufr27.miage.jpa.Train;
import fr.pantheonsorbonne.ufr27.miage.service.InfoCentreService;

public class InfoCentreServiceImpl implements InfoCentreService {
	@Inject
	EntityManager em;

	private TrainDAO trainDao;
	private PassagerDAO passagerDao;
	private TrainArretDAO trainArretDao;
	
	private TrainMapperCustom mapper = new TrainMapperCustom();

	public int createTrain(fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train trainDTO) {
		em.getTransaction().begin();
		Train train = mapper.getTrainEntiyFromDTO(trainDTO);
		em.persist(train);
		em.getTransaction().commit();

		return train.getIdTrain();
	}

	public int updateTrain(fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train trainDTO, int id) {
		Train oldTrain = em.find(Train.class, id);
		Train train = mapper.getTrainEntiyFromDTO(trainDTO);
		train.setPassagers(oldTrain.getPassagers());

		train.setIdTrain(id);
		int totalDelay = 0;

		for (Perturbation p : train.getPerturbations()) {
			totalDelay += p.getDuree();
		}

		if (oldTrain.getPerturbations().size() != train.getPerturbations().size()) {
			// update heures passage du train
			this.getTrainArretDao().delayHeuresDePassage(train, totalDelay, -1);

			/* update les heures de passages des autres trains impactes par ce retard */
			try {
				// on retard un peu plus les trains concernés pour pour assurer la
				// correspondance
				this.delayTrainsConcernesPerturbation(train, totalDelay + 10);
			} catch (NoTrainException e) {
				e.printStackTrace();
			}
		}

		if(!isRunningJUnitTest()) {
			this.sendAllTrainsInfoGares();
		}

		return train.getIdTrain();
	}
	
	private void delayTrainsConcernesPerturbation(final Train trainOriginal, final int delayMinutes) throws NoTrainException {
		if (!trainOriginal.isReservation()) {
			return;
		}

		List<Train> allTrains = this.getTrainDao().getAllTrains();

		for (Train t : allTrains) {
			if (t.getIdTrain() == trainOriginal.getIdTrain()) {
				continue;
			}

			if (this.getPassagerDao().getNumberOfSamePassagers(trainOriginal, t) > 50
					&& !t.getPerturbations().containsAll(trainOriginal.getPerturbations())) {

				t.getPerturbations().addAll(trainOriginal.getPerturbations());
				this.getTrainArretDao().delayHeuresDePassage(t, delayMinutes, -1);

			}
		}

	}

	public fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train getTrainFromId(int id) {
		Train train = em.find(Train.class, id);

		//update arretDesservi, if a train was supposed to stop more than 2 hours ago
		this.getTrainArretDao().updateArretsDesservis(train);

		// in first run(producer not set) we send all trains to infogares
		if (!isRunningJUnitTest() &&  Main.producer == null) {
			this.sendAllTrainsInfoGares();
		}

		return mapper.getTrainDTOFromEntity(train);
	}

	private void sendAllTrainsInfoGares() {
		try {
			Main.sendAllTrainsToInfoGare(this.getTrainDao().getAllTrains());
		} catch (InterruptedException | IOException | NoTrainException e) {
			e.printStackTrace();
		}
	}

	public static boolean isRunningJUnitTest() {  
	  for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
	    if (element.getClassName().startsWith("org.junit.")) {
	      return true;
	    }           
	  }
	  return false;
	}
	
	//for test classes
	public void setEm(EntityManager eman) {
		em = eman;
	}

	private TrainDAO getTrainDao() {
		if (trainDao == null) {
			trainDao = new TrainDAO(em);
		}

		return trainDao;
	}

	public PassagerDAO getPassagerDao() {
		if (passagerDao == null) {
			passagerDao = new PassagerDAO(em);
		}
		
		return passagerDao;
	}

	public TrainArretDAO getTrainArretDao() {
		if(trainArretDao == null) {
			trainArretDao = new TrainArretDAO(em);
		}
		
		return trainArretDao;
	}
}
