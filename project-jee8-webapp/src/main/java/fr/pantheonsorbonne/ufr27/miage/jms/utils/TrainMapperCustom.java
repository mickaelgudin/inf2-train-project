package fr.pantheonsorbonne.ufr27.miage.jms.utils;

import java.time.LocalDateTime;
import fr.pantheonsorbonne.ufr27.miage.jpa.Passager;
import fr.pantheonsorbonne.ufr27.miage.jpa.Perturbation;
import fr.pantheonsorbonne.ufr27.miage.jpa.Train;
import fr.pantheonsorbonne.ufr27.miage.jpa.TrainArret;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Arret;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * Class that configure mapping using orika
 */
public class TrainMapperCustom {
	
	
	/**
	 * transform Entity object to Jaxb object
	 * @param trainJpa
	 * @return trainDTO
	 */
	public fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train getTrainDTOFromEntity(Train trainJpa) {
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.getConverterFactory().registerConverter(new PassThroughConverter(LocalDateTime.class));
		
		mapperFactory.classMap(Train.class, fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train.class).byDefault()
				.register();

		MapperFacade mapper = mapperFactory.getMapperFacade();
		mapper.mapAsList(trainJpa.getTrainArrets(), fr.pantheonsorbonne.ufr27.miage.model.jaxb.TrainArret.class);
		mapper.mapAsList(trainJpa.getPassagers(), fr.pantheonsorbonne.ufr27.miage.model.jaxb.Passager.class);
		mapper.mapAsList(trainJpa.getPerturbations(), Perturbation.class);

		
		
		fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train trainDTO = mapper.map(trainJpa, fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train.class);
		
		int i = 0;
		for(TrainArret ta: trainJpa.getTrainArrets()) {
			Arret arret = new Arret();
			arret.setIdArret(ta.getArret().getIdArret());
			arret.setNomArret(ta.getArret().getNomArret());
			arret.setVille(ta.getArret().getVille());
			
			trainDTO.getTrainArrets().get(i++).setArret(arret);
		}
				
		return trainDTO;
	}


	/**
	 * transform Jaxb object to Entity object
	 * @param trainDto
	 * @return trainJpa
	 */
	public Train getTrainEntiyFromDTO(fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train trainDto) {
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.getConverterFactory().registerConverter(new PassThroughConverter(LocalDateTime.class));

		mapperFactory.classMap(fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train.class, Train.class).byDefault()
		.register();
		MapperFacade mapper = mapperFactory.getMapperFacade();
		mapper.mapAsList(trainDto.getTrainArrets(), TrainArret.class);
		mapper.mapAsList(trainDto.getPassagers(), Passager.class);
		mapper.mapAsList(trainDto.getPerturbations(), fr.pantheonsorbonne.ufr27.miage.jpa.Perturbation.class);
		Train trainJpa = mapper.map(trainDto, Train.class);

		int i = 0;
		for(TrainArret ta: trainJpa.getTrainArrets()) {
			fr.pantheonsorbonne.ufr27.miage.jpa.Arret arret = new fr.pantheonsorbonne.ufr27.miage.jpa.Arret();
			arret.setIdArret(ta.getArret().getIdArret());
			arret.setNomArret(ta.getArret().getNomArret());
			arret.setVille(ta.getArret().getVille());

			trainJpa.getTrainArrets().get(i++).setArret(arret);
		}

		return trainJpa;
	}
}
