package fr.pantheonsorbonne.ufr27.miage.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Passager {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idPassager;
	private String nom;
	private String prenom;
	@ManyToMany
	@JoinTable(name = "train_passagers", joinColumns = @JoinColumn(name = "idPassager"), inverseJoinColumns = @JoinColumn(name = "idTrain"))
	private List<Train> trains = new ArrayList<>();

	public int getIdPassager() {
		return idPassager;
	}

	public void setIdPassager(int idPassager) {
		this.idPassager = idPassager;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Train> getTrains() {
		return trains;
	}

	public void setTrains(List<Train> trains) {
		this.trains = trains;
	}

	
}
