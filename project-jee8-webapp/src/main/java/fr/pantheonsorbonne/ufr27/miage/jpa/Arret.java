package fr.pantheonsorbonne.ufr27.miage.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Arret {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idArret;
	private String nomArret;
	private String ville;

	@OneToMany(mappedBy = "arret", cascade = CascadeType.PERSIST)
	private List<TrainArret> trainArrets = new ArrayList<>();

	public int getIdArret() {
		return idArret;
	}

	public List<TrainArret> getTrainArrets() {
		return trainArrets;
	}

	public void setTrainArrets(List<TrainArret> trainArrets) {
		this.trainArrets = trainArrets;
	}

	public void setIdArret(int idArret) {
		this.idArret = idArret;
	}

	public String getNomArret() {
		return nomArret;
	}

	public void setNomArret(String nomArret) {
		this.nomArret = nomArret;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}
}
