package fr.pantheonsorbonne.ufr27.miage;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.github.javafaker.Faker;
import fr.pantheonsorbonne.ufr27.miage.jpa.Arret;
import fr.pantheonsorbonne.ufr27.miage.jpa.Passager;
import fr.pantheonsorbonne.ufr27.miage.jpa.Perturbation;
import fr.pantheonsorbonne.ufr27.miage.jpa.Train;
import fr.pantheonsorbonne.ufr27.miage.jpa.TrainArret;

public class DataInitialiser {

	public void init(EntityManager em) {
		LocalDateTime now = LocalDateTime.now();
		Faker faker = new Faker(new Locale("fr"));

		List<Passager> passagers = new ArrayList<>();

		// 54 PASSAGERS AVEC CORRESPONDANCE SONT CREE
		for (int i = 0; i < 54; i++) {
			passagers.add(getPassager(faker.name().lastName(), faker.name().firstName()));
		}

		Arret arretBordeaux = this.getArret("BORDEAUX ST JEAN", "BORDEAUX");
		Arret arretMontparrnasse = this.getArret("PARIS MONTPARNASSE", "PARIS");

		// -----------------TGV--------------------------------
		Train tgv = this.getTrain("TGV1", false, true, "TGV", 0, passagers);

		// ----------------TER----------------------------
		Train ter = this.getTrain("TER1", false, false, "TER", 0, passagers);

		Arret arretRambouillet = this.getArret("RAMBOUILLET", "RAMBOUILLET");
		Arret arretVersailles = this.getArret("VERSAILLES", "VERSAILLES");

		TrainArret trainarret1 = this.getTrainArret(now.plusMinutes(2), now.plusMinutes(2),
				now.plusHours(2).plusMinutes(2), now.plusHours(2).plusMinutes(2), true, tgv, arretBordeaux);
		TrainArret trainarret2 = this.getTrainArret(now.plusHours(2).plusMinutes(12), now.plusHours(2).plusMinutes(12),
				now.plusHours(2).plusMinutes(12), now.plusHours(2).plusMinutes(12), true, tgv, arretMontparrnasse);

		TrainArret trainArretMonpartnasse = this.getTrainArret(now.plusHours(2).plusMinutes(35),
				now.plusHours(2).plusMinutes(35), now.plusHours(2).plusMinutes(35), now.plusHours(2).plusMinutes(35),
				true, ter, arretMontparrnasse);
		TrainArret trainArretVersailles = this.getTrainArret(null, null, null, null, false, ter, arretVersailles);
		TrainArret trainArretRambouillet = this.getTrainArret(now.plusHours(2).plusMinutes(65),
				now.plusHours(2).plusMinutes(65), now.plusHours(2).plusMinutes(65), now.plusHours(2).plusMinutes(65),
				true, ter, arretRambouillet);

		this.addArretToTrain(ter, arretMontparrnasse, trainArretMonpartnasse);
		this.addArretToTrain(ter, arretVersailles, trainArretVersailles);
		this.addArretToTrain(ter, arretRambouillet, trainArretRambouillet);

		this.addArretToTrain(tgv, arretBordeaux, trainarret1);
		this.addArretToTrain(tgv, arretMontparrnasse, trainarret2);

		Train terParisVersailles = this.getTrain("TER2", true, false, "TER", 0, new ArrayList<>());
		TrainArret parisTa = this.getTrainArret(now.minusHours(2), now.plusHours(2), null, null, true,
				terParisVersailles, arretMontparrnasse);
		TrainArret versaillesTa = this.getTrainArret(now.minusHours(1), now.plusHours(3), now.minusHours(1),
				now.plusHours(3), true, terParisVersailles, arretVersailles);

		// on retard le ter Paris-Versailles de 2h05
		Perturbation p = new Perturbation();
		p.setType(2);
		p.setDateOccurence(now.minusHours(1));
		p.setDuree(185);
		terParisVersailles.getPerturbations().add(p);

		this.addArretToTrain(terParisVersailles, arretMontparrnasse, parisTa);
		this.addArretToTrain(terParisVersailles, arretVersailles, versaillesTa);

		em.getTransaction().begin();
		em.persist(tgv);
		em.persist(ter);
		em.persist(terParisVersailles);
		em.getTransaction().commit();

	}

	private Train getTrain(String numeroTrain, boolean arrete, boolean reservation, String typeTrain, int localisation,
			List<Passager> passagersTrain) {
		Train train = new Train();
		train.setNumeroTrain(numeroTrain);
		train.setArrete(arrete);
		train.setReservation(reservation);
		train.setTypeDeTrain(typeTrain);

		passagersTrain.forEach(passager -> train.getPassagers().add(passager));

		train.setLocalisation(localisation);

		return train;
	}

	private void addArretToTrain(Train t, Arret a, TrainArret ta) {
		t.getTrainArrets().add(ta);
		a.getTrainArrets().add(ta);
	}

	private TrainArret getTrainArret(LocalDateTime departTheorique, LocalDateTime departReel,
			LocalDateTime arriveeTheorique, LocalDateTime arriveeReel, boolean desservi, Train train, Arret arret) {

		TrainArret trainArret = new TrainArret();
		trainArret.setArriveeReel(arriveeReel);
		trainArret.setArriveeTheorique(arriveeTheorique);
		trainArret.setDepartReel(departReel);
		trainArret.setDepartTheorique(departTheorique);
		trainArret.setDesservi(desservi);
		trainArret.setTrain(train);
		trainArret.setArret(arret);

		return trainArret;
	}

	private Arret getArret(String nom, String ville) {
		Arret arret = new Arret();
		arret.setNomArret(nom);
		arret.setVille(ville);

		return arret;
	}

	private Passager getPassager(String nom, String prenom) {

		Passager passager = new Passager();
		passager.setPrenom(prenom);
		passager.setNom(nom);

		return passager;
	}
}
