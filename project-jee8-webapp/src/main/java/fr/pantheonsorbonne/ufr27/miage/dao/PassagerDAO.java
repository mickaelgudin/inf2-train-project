package fr.pantheonsorbonne.ufr27.miage.dao;

import java.util.List;

import javax.persistence.EntityManager;

import fr.pantheonsorbonne.ufr27.miage.jpa.Passager;
import fr.pantheonsorbonne.ufr27.miage.jpa.Train;

/**
 * DAO pour les passagers
 */
public class PassagerDAO {
	private EntityManager em;

	public PassagerDAO(EntityManager em) {
		super();
		this.em = em;
	}
	
	/**
	 * retourne le nombre de passager en commum entre deux train
	 * @param train1
	 * @param train2
	 * @return
	 */
	public int getNumberOfSamePassagers(Train train1, Train train2) {
		List<Passager> passagerT1 = train1.getPassagers();
		List<Passager> passagerT2 = train2.getPassagers();
		passagerT1.retainAll(passagerT2);
		return passagerT1.size();
	}
}
