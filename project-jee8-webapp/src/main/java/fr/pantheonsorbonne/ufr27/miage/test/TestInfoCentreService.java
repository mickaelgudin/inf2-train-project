package fr.pantheonsorbonne.ufr27.miage.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.net.URI;
import java.time.LocalDateTime;

import javax.persistence.EntityManager;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import fr.pantheonsorbonne.ufr27.miage.DataInitialiser;
import fr.pantheonsorbonne.ufr27.miage.Main;
import fr.pantheonsorbonne.ufr27.miage.conf.PersistenceConf;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Perturbation;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.TrainArret;
import fr.pantheonsorbonne.ufr27.miage.service.impl.InfoCentreServiceImpl;

public class TestInfoCentreService {
	static InfoCentreServiceImpl service;
	static HttpServer server;

	@BeforeClass
	public static void setUp() throws Exception {
		server = startServer();
		PersistenceConf pc = new PersistenceConf();
		EntityManager em = pc.getEM();

		DataInitialiser di = new DataInitialiser();
		di.init(em);
		service = new InfoCentreServiceImpl();
		service.setEm(em);
	}

	@AfterClass
	public static void stop() {
		server.stop();
	}

	@Test
	public void testCreateTrain() {
		Train train = new Train();
		train.setReservation(false);
		train.setNumeroTrain("TER5");
		train.setArrete(false);
		train.setTypeDeTrain("TER");
		int idOfNewTrain = service.createTrain(train);
		assertTrue(idOfNewTrain != 0);
	}

	@Test
	public void testUpdateTrain() {
		Train train = service.getTrainFromId(1);
		assertTrue(train.getPerturbations().isEmpty());
		
		Perturbation perturbation = new Perturbation();
		perturbation.setDateOccurence(LocalDateTime.now());
		perturbation.setDuree(20);
		perturbation.setType(5);
		train.getPerturbations().add(perturbation);
		
		service.updateTrain(train, 1);
		
		train = service.getTrainFromId(1);
		assertTrue(!train.getPerturbations().isEmpty() );
		
		//le train a bien été retardé
		assertEquals(train.getTrainArrets().get(0).getDepartReel() , train.getTrainArrets().get(0).getDepartTheorique().plusMinutes(20));
		
		//le ter est retarde par le tgv, avec un retard (10 minute + duree de la perturbation)
		Train ter = service.getTrainFromId(61);
		assertEquals(ter.getTrainArrets().get(0).getDepartReel() , ter.getTrainArrets().get(0).getDepartTheorique().plusMinutes(30));
	}

	@Test
	public void testGetTrainFromId() {
		Train train = service.getTrainFromId(61);
		assertTrue(train != null);
		assertTrue(train.getNumeroTrain().equals("TER1"));
		/*
		 * l arret de versailles est non desservi au debut, mais comme un train devait
		 * passe y a plus de 2h, le train va desservir versailles
		 */
		assertTrue(train.getTrainArrets().get(1).getArret().getNomArret().equals("VERSAILLES"));
		assertTrue(train.getTrainArrets().get(1).isDesservi());
	}

	public static HttpServer startServer() {

		return GrizzlyHttpServerFactory.createHttpServer(URI.create(Main.BASE_URI));
	}

}
