package fr.pantheonsorbonne.ufr27.miage.resource;

import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train;
import fr.pantheonsorbonne.ufr27.miage.service.InfoCentreService;

@Path("/train")
public class TrainEndpoint {
	@Inject
	InfoCentreService infoCentreService;

	@GET
	@Path("/{id}")
	@Produces(value = { MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response getTrain(@PathParam("id") int id) {
		return Response.ok(infoCentreService.getTrainFromId(id)).build();
	}

	@Produces(value = { MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes(value = { MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@POST
	public Response createTrain(Train train) throws URISyntaxException {
		int id = infoCentreService.createTrain(train);

		return Response.created(new URI("/train/" + id)).build();

	}

	@Produces(value = { MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes(value = { MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@PUT
	@Path("/{id}")
	public Response updateTrain(Train train, @PathParam("id") int id) throws URISyntaxException {
		int trainId = infoCentreService.updateTrain(train, id);

		return Response.created(new URI("/train/" + trainId)).build();

	}

}
