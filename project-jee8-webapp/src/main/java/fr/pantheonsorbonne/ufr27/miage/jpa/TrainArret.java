package fr.pantheonsorbonne.ufr27.miage.jpa;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "train_arret")
public class TrainArret implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idTrain")
	private Train train;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idArret")
	private Arret arret;

	private LocalDateTime departTheorique;
	private LocalDateTime departReel;
	private LocalDateTime arriveeTheorique;
	private LocalDateTime arriveeReel;
	private boolean desservi;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Train getTrain() {
		return train;
	}

	public void setTrain(Train train) {
		this.train = train;
	}

	public Arret getArret() {
		return arret;
	}

	public void setArret(Arret arret) {
		this.arret = arret;
	}

	public LocalDateTime getDepartTheorique() {
		return departTheorique;
	}

	public void setDepartTheorique(LocalDateTime departTheorique) {
		this.departTheorique = departTheorique;
	}

	public LocalDateTime getDepartReel() {
		return departReel;
	}

	public void setDepartReel(LocalDateTime departReel) {
		this.departReel = departReel;
	}

	public LocalDateTime getArriveeTheorique() {
		return arriveeTheorique;
	}

	public void setArriveeTheorique(LocalDateTime arriveeTheorique) {
		this.arriveeTheorique = arriveeTheorique;
	}

	public LocalDateTime getArriveeReel() {
		return arriveeReel;
	}

	public void setArriveeReel(LocalDateTime arriveeReel) {
		this.arriveeReel = arriveeReel;
	}

	public boolean isDesservi() {
		return desservi;
	}

	public void setDesservi(boolean desservi) {
		this.desservi = desservi;
	}
}
