package fr.pantheonsorbonne.ufr27.miage.service;

import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train;

public interface InfoCentreService {

	public int createTrain(Train train);

	public Train getTrainFromId(int id);

	public int updateTrain(Train train, int id);

}