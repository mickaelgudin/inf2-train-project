package fr.pantheonsorbonne.ufr27.miage.jpa;

import java.util.ArrayList;
import java.util.List;
import java.time.LocalDateTime;

import javax.persistence.*;

@Entity
public class Perturbation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int type;
	private int duree;
	private LocalDateTime dateOccurence;
	@ManyToMany
	@JoinTable(name = "train_perturbations", joinColumns = @JoinColumn(name = "idPerturbation"), inverseJoinColumns = @JoinColumn(name = "idTrain"))
	private List<Train> trains = new ArrayList<>();

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public LocalDateTime getDateOccurence() {
		return dateOccurence;
	}

	public void setDateOccurence(LocalDateTime dateOccurence) {
		this.dateOccurence = dateOccurence;
	}

	public List<Train> getTrains() {
		return trains;
	}

	public void setTrains(List<Train> trains) {
		this.trains = trains;
	}

}
