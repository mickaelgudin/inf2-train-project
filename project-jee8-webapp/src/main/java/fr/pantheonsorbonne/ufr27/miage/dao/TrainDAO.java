package fr.pantheonsorbonne.ufr27.miage.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import fr.pantheonsorbonne.ufr27.miage.exception.NoTrainException;
import fr.pantheonsorbonne.ufr27.miage.jpa.Train;
import fr.pantheonsorbonne.ufr27.miage.jpa.TrainArret;
import fr.pantheonsorbonne.ufr27.miage.jpa.Passager;

/**
 *	DAO pour les trains
 */
public class TrainDAO {

	private EntityManager em;

	public TrainDAO(EntityManager em) {
		super();
		this.em = em;
	}

	/**
	 * recupe tous les trains
	 * @return List<Train>
	 * @throws NoTrainException
	 */
	public List<Train> getAllTrains() throws NoTrainException {
		em.getTransaction().begin();
		List<Train> allTrain = em.createQuery("SELECT t FROM Train t", Train.class).getResultList();
		em.getTransaction().commit();
		if (allTrain == null) {
			throw new NoTrainException();
		}
		return allTrain;
	}

}
