package fr.pantheonsorbonne.ufr27.miage;

import java.io.Closeable;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import fr.pantheonsorbonne.ufr27.miage.jms.utils.TrainMapperCustom;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.TrainList;

public class InfoCentreProducer implements Closeable {

	@Inject
	@Named("infoTrain")
	Topic topic;

	@Inject
	ConnectionFactory connectionFactory;

	Connection connection;
	Session session;
	MessageProducer messageProducer;

	boolean firstProduce = true; // check if we are sending initial infos

	TrainMapperCustom mapper = new TrainMapperCustom();

	@PostConstruct
	private void init() {
		try {
			this.connection = connectionFactory.createConnection("nicolas", "nicolas");
			connection.start();
			this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			this.messageProducer = session.createProducer(topic);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}

	}

	public String produce(Train trains) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Train.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(trains, sw);

			this.messageProducer.send(this.session.createTextMessage(sw.toString()));
			return sw.toString();
		} catch (JMSException | JAXBException e) {
			System.out.println("Failed to send message to queue");
			return "Nothing sent";
		}
	}

	public String produceAllTrains(List<fr.pantheonsorbonne.ufr27.miage.jpa.Train> trains) {

		JAXBContext jaxbContext;
		Marshaller jaxbMarshaller = null;
		try {
			jaxbContext = JAXBContext.newInstance(TrainList.class);
			jaxbMarshaller = jaxbContext.createMarshaller();
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		TrainList listOfTrains = new TrainList();

		for (fr.pantheonsorbonne.ufr27.miage.jpa.Train train : trains) {
			listOfTrains.getTrains().add(mapper.getTrainDTOFromEntity(train));
		}
		StringWriter sw = new StringWriter();
		try {
			if (jaxbMarshaller != null)
				jaxbMarshaller.marshal(listOfTrains, sw);

			TextMessage message = this.session.createTextMessage(sw.toString());

			if (firstProduce) {
				message.setStringProperty("typeOfMessage", "INFOS D'ORIGINE");
				firstProduce = false;
			} else {
				message.setStringProperty("typeOfMessage", "UPDATE");
			}

			this.messageProducer.send(message);
		} catch (JAXBException | JMSException e) {
			e.printStackTrace();
		}

		return sw.toString();

	}

	@Override
	public void close() throws IOException {
		try {
			messageProducer.close();
			session.close();
			connection.close();
		} catch (JMSException e) {
			System.out.println("failed to close JMS resources");
		}

	}

}
