package fr.pantheonsorbonne.ufr27.miage.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Train {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idTrain;
	private String numeroTrain;
	private int localisation;
	private boolean arrete;
	private boolean reservation;
	private String typeDeTrain;
	
	@OneToMany(mappedBy = "train",cascade = CascadeType.PERSIST )
	private List<TrainArret> trainArrets = new ArrayList<>();

	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "train_passagers", joinColumns = @JoinColumn(name = "idTrain"), inverseJoinColumns = @JoinColumn(name = "idPassager"))
	private List<Passager> passagers = new ArrayList<>();

	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "train_perturbations", joinColumns = @JoinColumn(name = "idTrain"), inverseJoinColumns = @JoinColumn(name = "idPerturbation"))
	private List<Perturbation> perturbations = new ArrayList<>();

	public List<TrainArret> getTrainArrets() {
		return trainArrets;
	}

	public void setTrainArrets(List<TrainArret> trainArrets) {
		this.trainArrets = trainArrets;
	}

	public int getIdTrain() {
		return idTrain;
	}

	public void setIdTrain(int idTrain) {
		this.idTrain = idTrain;
	}

	public String getNumeroTrain() {
		return numeroTrain;
	}

	public void setNumeroTrain(String numeroTrain) {
		this.numeroTrain = numeroTrain;
	}

	public int getLocalisation() {
		return localisation;
	}

	public void setLocalisation(int localisation) {
		this.localisation = localisation;
	}

	public boolean isArrete() {
		return arrete;
	}

	public void setArrete(boolean arrete) {
		this.arrete = arrete;
	}

	public boolean isReservation() {
		return reservation;
	}

	public void setReservation(boolean reservation) {
		this.reservation = reservation;
	}

	public String getTypeDeTrain() {
		return typeDeTrain;
	}

	public void setTypeDeTrain(String typeDeTrain) {
		this.typeDeTrain = typeDeTrain;
	}

	public List<Passager> getPassagers() {
		return passagers;
	}

	public void setPassagers(List<Passager> passagers) {
		this.passagers = passagers;
	}

	public List<Perturbation> getPerturbations() {
		return perturbations;
	}

	public void setPerturbations(List<Perturbation> perturbations) {
		this.perturbations = perturbations;
	}

}
