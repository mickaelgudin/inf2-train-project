package fr.pantheonsorbonne.ufr27.miage.dao;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import fr.pantheonsorbonne.ufr27.miage.exception.NoTrainException;
import fr.pantheonsorbonne.ufr27.miage.jpa.Train;
import fr.pantheonsorbonne.ufr27.miage.jpa.TrainArret;

/**
 * DAO pour les heures de passage
 */
public class TrainArretDAO {
	private EntityManager em;

	public TrainArretDAO(EntityManager em) {
		super();
		this.em = em;
	}
	
	/**
	 * Permet de verifier si arret devait etre desservi y a plus de 2h (la liste est vide si c'est pas le cas)
	 * @param trainArret
	 * @return
	 */
	private List<TrainArret> getArretPasDesservi(TrainArret trainArret) {
		Query queryTrainArrets = em.createNativeQuery(
				"SELECT * FROM Train_Arret WHERE DATEDIFF(MINUTE, DEPARTTHEORIQUE, DEPARTREEL) > 120 AND IDARRET = ? ",
				TrainArret.class).setParameter(1, trainArret.getArret().getIdArret());
		return queryTrainArrets.getResultList();
	}
	
	/**
	 * update la dessert d'un arret sur le trajet d'un train(non desservi avant) 
	 * si l arret devait etre desservis y a plus de 2h
	 * @param train
	 */
	public void updateArretsDesservis(final Train train) {

		em.getTransaction().begin();
		for (TrainArret trainArret : train.getTrainArrets()) {
			if (trainArret.isDesservi()) {
				continue;
			}
			
			final List<TrainArret> listTrainArrets = this.getArretPasDesservi(trainArret);
			
			if (!listTrainArrets.isEmpty()) {
				LocalDateTime now = LocalDateTime.now();
				trainArret.setDesservi(true);
				trainArret.setDepartTheorique(now.plusMinutes(20));
				trainArret.setDepartReel(now.plusMinutes(20));
				trainArret.setArriveeReel(now.plusMinutes(20));
				trainArret.setArriveeTheorique(now.plusMinutes(20));
				em.merge(trainArret);

				// la nouvelle dessert de l'arret va changer la dessert des prochains
				long multiplierMinutes = 0;
				for (int i = train.getTrainArrets().indexOf(trainArret) + 1; i < train.getTrainArrets().size(); i++) {
					TrainArret currentTrainArret = train.getTrainArrets().get(i);
					if (currentTrainArret.isDesservi()) {
						currentTrainArret.setDepartTheorique(now.plusMinutes(20 * multiplierMinutes));
						currentTrainArret.setDepartReel(now.plusMinutes(20 * multiplierMinutes));
						currentTrainArret.setArriveeReel(now.plusMinutes(20 * multiplierMinutes));
						currentTrainArret.setArriveeTheorique(now.plusMinutes(20 * multiplierMinutes));
						em.merge(trainArret);
						multiplierMinutes++;
					}
				}

			}
		}
		em.flush();
		em.getTransaction().commit();
	}
	
	/**
	 * Retarde les heures de passage d'un train(nouvelle perturbation par ex)
	 * @param train
	 * @param totalDelayMinutes
	 * @param indexToStart
	 */
	public void delayHeuresDePassage(Train train, int totalDelayMinutes, final int indexToStart) {
		em.getTransaction().begin();

		for (int i = (indexToStart == -1) ? train.getLocalisation() : indexToStart; i < train.getTrainArrets()
				.size(); i++) {

			if (train.getTrainArrets().get(i).isDesservi()) {

				train.getTrainArrets().get(i).setDepartReel(
						train.getTrainArrets().get(i).getDepartTheorique().plusMinutes(totalDelayMinutes));
				train.getTrainArrets().get(i).setArriveeReel(
						train.getTrainArrets().get(i).getArriveeTheorique().plusMinutes(totalDelayMinutes));

				em.merge(train.getTrainArrets().get(i));
			}
		}
		em.merge(train);
		em.flush();
		em.getTransaction().commit();

	}
	
	/**
	 * Ajoute un arret avec l'heure de dessert à un train
	 * @param idTrain
	 * @param trainArretAjoute
	 * @param indexToBeAdded
	 * @throws NoTrainException
	 */
	public void addArretToTrain(final int idTrain, final TrainArret trainArretAjoute, final int indexToBeAdded)
			throws NoTrainException {
		em.getTransaction().begin();
		Train train = em.find(Train.class, idTrain);
		if (train != null) {
			List<TrainArret> trainArrets = train.getTrainArrets();
			trainArrets.add(indexToBeAdded, trainArretAjoute);
			train.setTrainArrets(trainArrets);
		} else {
			throw new NoTrainException();
		}
		em.persist(train);
		em.getTransaction().commit();
	}
	
	/**
	 * Supprimer un arret desservi par un train
	 * @param idTrain
	 * @param trainArret
	 * @throws NoTrainException
	 */
	public void deleteArretToTrain(final int idTrain, TrainArret trainArret) throws NoTrainException {
		em.getTransaction().begin();
		Train train = em.find(Train.class, idTrain);
		if (train != null) {
			List<TrainArret> trainArrets = train.getTrainArrets();
			trainArrets.remove(trainArret);
			train.setTrainArrets(trainArrets);
		} else {
			throw new NoTrainException();
		}
		em.persist(train);
		em.getTransaction().commit();
	}
	
}
