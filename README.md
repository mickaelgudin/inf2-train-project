See the diagram in the image below to understand the subprojects interractions and the project meaning.

## Running

### project-jee8

This is the parent projet

### project-jee8-model

This project containts sur JAXB generated DTO to be used for communication in the REST, JMS and Business Layer

### project-jee8-webapp

This project contains a REST API (JAXRS), the persistence layer (JPA), the service layer and make use of Messaging Queue (JMS),
It's the infocentre that receives infos about disruptions sent by trains(project project-jee8-rest-client)
and send all infos to "trains stations" using a JMS TOPIC(info to each "trains stations" in the project project-jee8-jms-subscriber)

### project-jee8-rest-client
Simulate several trains sending info(disruptions...) each 5 seconds to the infocentre(project-jee8-webapp) using REST API calls(implemented with Jersey Client for JAXRS)

### project-jee8-jms-subscriber
This project consumes message produced by project-jee8-webapp, "train stations" filter info to display by the given timetable display screen

## Diagram

![](sec.png)
