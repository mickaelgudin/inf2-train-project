package fr.pantheonsorbonne.ufr27.miage;

import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Perturbation;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.TrainArret;

public class Train implements Runnable {
	int id;
	int time = 0;
	int delay = 0;
	fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train trainDTO = new fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train();
	private long minutesBeforeNextStep = 0; // avant depart ou arrivee
	private int currentNbPerturbations = 0;
	private int currentArret = 0;

	class RetrieveRecordTask extends TimerTask {
		public void run() {
			retrieveRecord();
		}
	}

	Train(int id) {
		this.id = id;
		this.retrieveRecord();
		this.minutesBeforeNextStep = LocalDateTime.now().until(this.trainDTO.getTrainArrets().get(0).getDepartReel(),
				ChronoUnit.MINUTES);
	}

	/**
	 * On recupere le DTO de l'infoCentre
	 */
	public void retrieveRecord() {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8080/train/" + String.valueOf(this.id));
		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_XML);
		
		StringReader response = new StringReader(invocationBuilder.get(String.class));
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			this.trainDTO = (fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train) jaxbUnmarshaller.unmarshal(response);

			System.out.println("\n\n\tGET sur INFOCENTRE - TRAIN " + this.trainDTO.getNumeroTrain());
			System.out.println("----------------------------------------------------\n\n");
		} catch (JAXBException e1) {
			e1.printStackTrace();
		}

		//si des nouvelles perturbations sont rencontres
		if (this.trainDTO.getPerturbations().size() > this.currentNbPerturbations) {
			this.addDelayLastNPerturbations(this.trainDTO.getPerturbations().size() - this.currentNbPerturbations);
			this.currentNbPerturbations = this.trainDTO.getPerturbations().size();
		}
		
	}

	public void updateRecord() {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8080/train/" + String.valueOf(this.id));
		target.request(MediaType.APPLICATION_XML);

		target.request().put(Entity.entity(this.trainDTO, MediaType.APPLICATION_XML));

	}

	public void addDelayLastNPerturbations(int n) {
		int cpt = 0;

		for (int i = this.trainDTO.getPerturbations().size() - 1; cpt++ < n; i++) {
			this.minutesBeforeNextStep += this.trainDTO.getPerturbations().get(i).getDuree();
		}
	}

	@Override
	public void run() {
		// MAJ DU DTO TOUTES LES 5 SECONDES
		Timer timer = new Timer();
		timer.schedule(new RetrieveRecordTask(), 0, 5000);

		while (true) {
			LocalDateTime now = LocalDateTime.now();

			if (this.getId() == 1 && time == 6) {
				Perturbation p = new Perturbation();
				p.setDateOccurence(now);
				p.setDuree(50);
				p.setType(1);

				// on ajoute les durée des nouvelles perturbations
				System.out.println("\n-----------------------------------------------------------");
				System.out.println("\tLE TRAIN " + this.trainDTO.getNumeroTrain() + " EST RETARDE DE " + p.getDuree()
						+ " MINUTES");
				System.out.println("\tLes trains seront maj a leur prochain GET respectif\t\t");
				System.out.println("-----------------------------------------------------------\n");

				this.trainDTO.getPerturbations().add(p);
				this.updateRecord();
			}

			time++;
			try {
				if (this.minutesBeforeNextStep > 0) {

					String prefixMessage = (this.currentArret == 0)
							? "TRAIN " + this.trainDTO.getNumeroTrain() + " partira de la gare "
							: "TRAIN " + this.trainDTO.getNumeroTrain() + " arrivera en gare de ";

					System.out.println(prefixMessage
							+ this.trainDTO.getTrainArrets().get(this.currentArret).getArret().getNomArret() + " dans "
							+ this.minutesBeforeNextStep + " minutes");
					this.minutesBeforeNextStep -= 10; // 1 seconde = 10 minutes
				} else if ((this.currentArret + 1) < this.trainDTO.getTrainArrets().size()) {
					this.currentArret++;

					// on zappe les arrets non desservi
					while (!this.trainDTO.getTrainArrets().get(this.currentArret).isDesservi()) {
						this.currentArret++;
					}

					if (this.currentArret < this.trainDTO.getTrainArrets().size()) {
						System.out.println("-----------------------------------------------------------|\n" + "\tTrain "
								+ this.trainDTO.getNumeroTrain() + "	- PROCHAIN ARRET : "
								+ this.trainDTO.getTrainArrets().get(this.currentArret).getArret().getNomArret()
								+ "       \n" + "-----------------------------------------------------------|");

						this.minutesBeforeNextStep = LocalDateTime.now().until(
								this.trainDTO.getTrainArrets().get(this.currentArret).getArriveeReel(),
								ChronoUnit.MINUTES);
						this.trainDTO.setLocalisation(currentArret);
						// update la localisation
						this.updateRecord();
					}
				} else {
					System.out.println("-----------------------------------------------------------------------");
					System.out.println("\t\tLE TRAIN " + this.trainDTO.getNumeroTrain() + " EST ARRIVE AU TERMINUS : "
							+ this.trainDTO.getTrainArrets().get(this.trainDTO.getTrainArrets().size() - 1).getArret()
									.getNomArret());
					System.out.println("-----------------------------------------------------------------------");
					timer.cancel();
					Thread.currentThread().interrupt();
					return;
				}

				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Train [id=" + id + ", delay=" + delay + "]";
	}

}