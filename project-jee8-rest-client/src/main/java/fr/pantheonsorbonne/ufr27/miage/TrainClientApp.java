package fr.pantheonsorbonne.ufr27.miage;

public class TrainClientApp {

	public static void main(String[] args) throws InterruptedException {
		Train t1 = new Train(1);
		Train t2 = new Train(61);
		
		new Thread(t1).start();
		//synchorisation : evite que le t2 parte avant le t1, alors que le t2 a bien ete retarde
		Thread.sleep(2000); 
		new Thread(t2).start();
		
		try {
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
