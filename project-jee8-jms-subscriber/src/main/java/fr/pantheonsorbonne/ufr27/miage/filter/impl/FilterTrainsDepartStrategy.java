package fr.pantheonsorbonne.ufr27.miage.filter.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.pantheonsorbonne.ufr27.miage.filter.interf.FilterInfoGareStrategy;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.TrainArret;

/**
 * class used to filter only trains that depart from a given arret
 * the last stop is excluded from the departures for each trains
 */
public class FilterTrainsDepartStrategy implements FilterInfoGareStrategy {

	@Override
	public List<Train> filtrer(List<Train> trains, String nomArret) {
		List<Train> trainsDepart = new ArrayList<Train>();
		
		for(Train t: trains) {
			int sizeTrainsArrets = t.getTrainArrets().size();
			int index = 0;
			
			for(TrainArret ta: t.getTrainArrets()) {
				if(index++ == sizeTrainsArrets-1) break;
				
				if(ta.getArret().getNomArret().equals(nomArret) ) {
					trainsDepart.add(t);
					break;
				}
			}
		}
		
		return trainsDepart;
	}

}
