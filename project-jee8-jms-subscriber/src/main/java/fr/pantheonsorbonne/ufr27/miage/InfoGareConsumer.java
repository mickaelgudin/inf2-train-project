package fr.pantheonsorbonne.ufr27.miage;

import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import fr.pantheonsorbonne.ufr27.miage.filter.impl.FilterPerturbationsStrategyImpl;
import fr.pantheonsorbonne.ufr27.miage.filter.impl.FilterTrainsArriveeStrategy;
import fr.pantheonsorbonne.ufr27.miage.filter.impl.FilterTrainsDepartStrategy;
import fr.pantheonsorbonne.ufr27.miage.filter.interf.FilterInfoGareStrategy;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Perturbation;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.TrainArret;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.TrainList;

public class InfoGareConsumer implements Closeable {

	@Inject
	@Named("infoTrain")
	private Topic topic;

	@Inject
	private ConnectionFactory connectionFactory;

	private Connection connection;
	private MessageConsumer messageConsumer;

	private String arretName;
	
	private String typeInfoGare; // departs, arrivees ou perturbations
	
	private FilterInfoGareStrategy filterStrategy;

	@PostConstruct
	void init() {
		try {
			connection = connectionFactory.createConnection("nicolas", "nicolas");
			connection.setClientID("InfoGare : " + UUID.randomUUID());
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			messageConsumer = session.createDurableConsumer(topic, "info-train-topic");
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}

	}

	public String consumeTrains() throws JAXBException, JMSException, XMLStreamException, FactoryConfigurationError {
		JAXBContext jaxbContext = JAXBContext.newInstance(TrainList.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Message message = messageConsumer.receive();

		if (message != null) {
			String text = ((TextMessage) message).getText();
			StringBuilder affichage = new StringBuilder();
			text = text.replace("Message: ", "");

			// on affiche si l'info est la premiere info ou si c'est un UPDATE
			affichage.append("-----"+message.getStringProperty("typeOfMessage")+" : "+this.typeInfoGare+" - "+this.arretName+ "--------\n");
			
			
			StringReader reader = new StringReader(text);
			XMLStreamReader readerXml = XMLInputFactory.newInstance().createXMLStreamReader((Reader) reader);

			TrainList trainListDTO = (TrainList) jaxbUnmarshaller.unmarshal(readerXml);
			
			List<Train> trainsFiltered = this.filterInfos(trainListDTO.getTrains());
			
			if(trainsFiltered.isEmpty()) {
				affichage.append("Pas d'informations");
				
			} else if (this.typeInfoGare.equals("perturbations")) {
				/*ici toutes les perturbations sont affichés, 
				 * dans des conditions reel il y aurait un filtre que sur 
				 * les pertubations graves
				 */
				for (Train t : trainsFiltered) {
					for(Perturbation p: t.getPerturbations()) {
						affichage.append("\nPertubation - (Train "+t.getNumeroTrain()+") : ");
						affichage.append("type : " + p.getType() + "; duree : "+p.getDuree()+" minutes" );
					}
				}
			} else {
				for (Train t : trainsFiltered) {
					affichage.append("\nTRAIN : ");
					affichage.append("Numero : " + t.getNumeroTrain() + ", ");
					// on affiche les depart ou les arrivee de la gare
					if (t.getTrainArrets() != null && !t.getTrainArrets().isEmpty()) {
						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
						
						if(this.typeInfoGare.equals("departs")) {
							affichage.append("Depart : " + t.getTrainArrets().get(0).getDepartReel().format(formatter).replace(":", "H") + ", ");
						}
						else if(this.typeInfoGare.equals("arrivees")) {
							affichage.append("Arrivée : " + t.getTrainArrets().get(0).getArriveeReel().format(formatter).replace(":", "H") + ", ");
						}
						
					}
					
					int minutesRetard = 0;
					for(Perturbation p: t.getPerturbations()) {
						minutesRetard += p.getDuree();
					}
					
					if(minutesRetard > 0) {
						affichage.append("Retard : " +minutesRetard+" minutes");
					} else {
						affichage.append("Pas de retard");
					}
					
					affichage.append("\n");
				}
			}

			affichage.append("\n----------------------------------------------\n\n");
			return affichage.toString();
		}

		return "rien";
	}

	@Override
	public void close() throws IOException {
		try {
			messageConsumer.close();
			connection.close();
		} catch (JMSException e) {
			System.out.println("Failed to close JMS resources");
		}

	}

	public void setArret(String arret) {
		this.arretName = arret;
	}

	public void setTypeInfoGare(String typeInfoGare) {
		if (!typeInfoGare.equals("departs") && !typeInfoGare.equals("arrivees")
				&& !typeInfoGare.equals("perturbations")) {
			return;
		}

		this.typeInfoGare = typeInfoGare;
	}

	private List<Train> filterInfos(List<Train> trains) {
		if (this.typeInfoGare.equals("departs")) {
			this.filterStrategy = new FilterTrainsDepartStrategy();
		} else if (this.typeInfoGare.equals("arrivees")) {
			this.filterStrategy = new FilterTrainsArriveeStrategy();
		} else if (this.typeInfoGare.equals("perturbations")) {
			this.filterStrategy = new FilterPerturbationsStrategyImpl();
		}

		if(filterStrategy != null) {
			return this.filterStrategy.filtrer(trains, this.arretName);
		}
		
		return new ArrayList<Train>();
	}
}
