package fr.pantheonsorbonne.ufr27.miage.filter.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.pantheonsorbonne.ufr27.miage.filter.interf.FilterInfoGareStrategy;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.TrainArret;

/**
 * class used to filter only trains that arrives in a given infogare
 * only the last stop of each train is considered to be an arrival
 */
public class FilterTrainsArriveeStrategy implements FilterInfoGareStrategy {

	@Override
	public List<Train> filtrer(List<Train> trains, String nomArret) {
		List<Train> trainsArrive = new ArrayList<Train>();
		
		for(Train t: trains) {
			if(!t.getTrainArrets().isEmpty()) {
				TrainArret ta = t.getTrainArrets().get(t.getTrainArrets().size() - 1);
				if(ta.getArret().getNomArret().equals(nomArret) ) {
					trainsArrive.add(t);
				}
			}
		}
		
		return trainsArrive;
	}

}
