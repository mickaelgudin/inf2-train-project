package fr.pantheonsorbonne.ufr27.miage;

import javax.enterprise.inject.se.SeContainer;
import javax.enterprise.inject.se.SeContainerInitializer;
import javax.jms.JMSException;
import javax.xml.bind.JAXBException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

public class InfoGareApp {
	public static void main(String[] args) throws JAXBException, JMSException, XMLStreamException, FactoryConfigurationError {
		SeContainerInitializer initializer = SeContainerInitializer.newInstance();

		try (SeContainer container = initializer.disableDiscovery().addPackages(InfoGareApp.class).initialize()) {
			String parisMontparnasseName = "PARIS MONTPARNASSE";
			
			//infos gares Paris
			final InfoGareConsumer infoGareDepartsParis = container.select(InfoGareConsumer.class).get();
			infoGareDepartsParis.setArret(parisMontparnasseName);
			infoGareDepartsParis.setTypeInfoGare("departs");
			final InfoGareConsumer infoGareArriveesParis = container.select(InfoGareConsumer.class).get();
			infoGareArriveesParis.setArret(parisMontparnasseName);
			infoGareArriveesParis.setTypeInfoGare("arrivees");
			final InfoGareConsumer infoGarePerturbationsParis = container.select(InfoGareConsumer.class).get();
			infoGarePerturbationsParis.setArret(parisMontparnasseName);
			infoGarePerturbationsParis.setTypeInfoGare("perturbations");
			
			
			//infos gares Bordeaux
			final InfoGareConsumer infoGareBordeaux = container.select(InfoGareConsumer.class).get();
			infoGareBordeaux.setArret("BORDEAUX ST JEAN");
			infoGareBordeaux.setTypeInfoGare("departs");
			
			//infos gares Rambouillet
			final InfoGareConsumer infoGareArriveesRambouillet = container.select(InfoGareConsumer.class).get();
			infoGareArriveesRambouillet.setArret("RAMBOUILLET");
			infoGareArriveesRambouillet.setTypeInfoGare("arrivees");
			
			while(true) {
				//infos gares Paris
				System.out.println(infoGareDepartsParis.consumeTrains());
				System.out.println(infoGareArriveesParis.consumeTrains());
				System.out.println(infoGarePerturbationsParis.consumeTrains());
				
				//infos gares Bordeaux
				System.out.println(infoGareBordeaux.consumeTrains());
				
				//infos gares Rambouillet
				System.out.println(infoGareArriveesRambouillet.consumeTrains());
				
			}
			
		} 
	}
}
