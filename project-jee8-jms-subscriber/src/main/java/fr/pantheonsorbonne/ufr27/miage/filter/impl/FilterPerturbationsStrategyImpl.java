package fr.pantheonsorbonne.ufr27.miage.filter.impl;

import java.util.ArrayList;

import java.util.List;
import fr.pantheonsorbonne.ufr27.miage.filter.interf.FilterInfoGareStrategy;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.TrainArret;

/**
 * returns trains that contains pertubations for the given arret
 * (it allow us to display the number of the train for a given perturbation)
 */
public class FilterPerturbationsStrategyImpl implements FilterInfoGareStrategy {

	@Override
	public List<Train> filtrer(List<Train> trains, String nomArret) {
		List<Train> trainsFiltered = new ArrayList<>();

		for (Train t : trains) {
			for (TrainArret ta : t.getTrainArrets()) {
				if (ta.getArret().getNomArret().equals(nomArret)) {
					trainsFiltered.add(t);
					break;
				}
			}
		}

		return trainsFiltered;
	}
}
