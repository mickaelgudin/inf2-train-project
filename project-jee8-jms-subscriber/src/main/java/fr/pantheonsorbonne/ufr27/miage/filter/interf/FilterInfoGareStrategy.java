package fr.pantheonsorbonne.ufr27.miage.filter.interf;

import java.util.List;
import fr.pantheonsorbonne.ufr27.miage.model.jaxb.Train;

public interface FilterInfoGareStrategy{
	List<Train> filtrer(List<Train> trains, String nomArret);
}
